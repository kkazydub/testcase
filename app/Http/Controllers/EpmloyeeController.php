<?php

namespace App\Http\Controllers;

use App\Epmloyee;
use Illuminate\Http\Request;

class EpmloyeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Epmloyee  $epmloyee
     * @return \Illuminate\Http\Response
     */
    public function show(Epmloyee $epmloyee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Epmloyee  $epmloyee
     * @return \Illuminate\Http\Response
     */
    public function edit(Epmloyee $epmloyee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Epmloyee  $epmloyee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Epmloyee $epmloyee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Epmloyee  $epmloyee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Epmloyee $epmloyee)
    {
        //
    }
}
