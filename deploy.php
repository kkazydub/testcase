<?php
namespace Deployer;

require 'recipe/laravel.php';

// Set PHP version for Run "composer install"
set('bin/php', function () {
    return '/usr/local/php71/bin/php';
});

// Set type of chmod due to UKRAINE.COM.UA
set('writable_mode', 'chmod');

// Project name(directory in which repository will be cloned should if folder no WWW)

// Project repository
set('repository', 'https://kkazydub@bitbucket.org/kkazydub/testcase.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Hosts

host('kkazydub.ftp.tools')
    ->user('kkazydub')
    ->set('http_user', 'kkazydub')
    ->identityFile('~/.ssh/deploy_test')
    ->set('deploy_path', '/home/kkazydub/kazik.website/www')
    ->forwardAgent()
;

// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');
